library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity control_motor is
port(
  sentido_motor: in std_logic;
  power_motor: in std_logic;
  led_motor: out std_logic_vector(1 downto 0)
);
end control_motor;

architecture behavioral of control_motor is
begin
  process (power_motor, sentido_motor)
  begin
    if (power_motor = '1') then 
      if sentido_motor = '0' then
        led_motor <= "01";
      else
        led_motor <= "10";
      end if;
    else 
        led_motor <="00";
end if;
end process;
  
end architecture;