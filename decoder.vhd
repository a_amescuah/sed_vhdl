library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


ENTITY decoder IS
PORT (
  code : IN std_logic_vector(3 DOWNTO 0);
  led : OUT std_logic_vector(6 DOWNTO 0)
);
END ENTITY decoder;

ARCHITECTURE dataflow OF decoder IS
BEGIN
  WITH code SELECT
  led <=  "1111001" WHEN "1000",--"1001111" WHEN "1000", --planta 1
          "0100100" WHEN "0100", --planta 2
          "0110000" WHEN "0010", --planta 3
          "0011001" WHEN "0001", --planta 4
          "0111111" WHEN others;--señal de error en lectura de planta, es un -
END ARCHITECTURE dataflow;
