library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fsm is
 port (
 SWITCH_EMERGENCIA  : in  std_logic;
 CLK                : in  std_logic;
 SENSOR_PLANTA      : in  std_logic_vector(3 downto 0);
 PUSHBUTTON         : in  std_logic_vector(3 downto 0);
 bcd                : out std_logic_vector(3 downto 0);
 led                : out std_logic_vector(2 downto 0);
 Motor_Direccion    : out std_logic;
 Marcha_Paro        : out std_logic
 );
end fsm;


architecture behavioral of fsm is
 type STATES is (S1, S2, S3, S4, SUBIENDO1a2, SUBIENDO2a3, SUBIENDO3a4, BAJANDO2a1, BAJANDO3a2, BAJANDO4a3, EMERGENCIA);
 signal current_state: STATES := S1;
 signal next_state: STATES;
 signal aux : std_logic_vector(2 downto 0); --Indica la planta objetivo.
 function Siguiente_Estado(
    aux : std_logic_vector (2 downto 0) := "000";
    PUSHBUTTON : std_logic_vector (3 downto 0)
) return std_logic_vector is variable next_aux: std_logic_vector (3 downto 0);
begin
    if PUSHBUTTON = "1000" then
    return "000";
    elsif PUSHBUTTON = "0100" then
    return "001";
    elsif PUSHBUTTON = "0010" then
    return "010";
    elsif PUSHBUTTON = "0001" then
    return "011";
    else
    return aux;
    end if;
end function;
begin
--PISO 1 -- (3)
--PISO 2 -- (2)
--PISO 3 -- (1)
--PISO 4 -- (0)
 state_register: process (SWITCH_EMERGENCIA, CLK)
 begin
 --Completar
 if SWITCH_EMERGENCIA = '1' then 
    current_state <= EMERGENCIA;       --Tratamiento señal emergencia
 
 elsif clk = '1' and clk'event then
    current_state <= next_state; 
 end if;
 end process;
 
  nextstate_decod: process (current_state,PUSHBUTTON,SENSOR_PLANTA)
begin
 next_state <= current_state;
 case current_state is
--Piso 1
 when S1 =>
 if aux ="000" then
 aux <= Siguiente_Estado(aux, PUSHBUTTON);
 elsif aux = "000" then                       -- En caso de que el ascensor tenga que bajar más plantas, se reinicia el auxiliar 
 next_state <= S1;
 elsif aux > "000" then                       -- En caso de que el ascensor tenga que subir más plantas
 next_state <= subiendo1a2;
 end if;
 
 --Piso 2
 when S2 =>
 if aux ="001" then
 aux <= Siguiente_Estado(aux, PUSHBUTTON);
 elsif aux < "001" then                       -- En caso de que el ascensor tenga que bajar más plantas, se reinicia el auxiliar 
 next_state <= bajando2a1;
 elsif aux > "001" then                       -- En caso de que el ascensor tenga que subir más plantas
 next_state <= subiendo2a3;
 end if;
 
 --Piso 3
 when S3 =>
 if aux ="010" then
 aux <= Siguiente_Estado(aux, PUSHBUTTON);
 elsif aux < "010" then                       -- En caso de que el ascensor tenga que bajar más plantas, se reinicia el auxiliar 
 next_state <= bajando3a2;
 elsif aux > "010" then                       -- En caso de que el ascensor tenga que subir más plantas
 next_state <= subiendo3a4;
 end if;
 
 --Piso 4
 when S4 =>
 if aux ="011" then
 aux <= Siguiente_Estado(aux, PUSHBUTTON);
 elsif aux < "011" then                       -- En caso de que el ascensor tenga que bajar más plantas, se reinicia el auxiliar 
 next_state <= bajando4a3;
 elsif aux > "011" then                       -- En caso de que el ascensor tenga que subir más plantas
 next_state <= S4;
 end if;
    -- Subir --
 --Piso 1 a Piso 2
 when subiendo1a2 =>
 if SENSOR_PLANTA(2) = '1' then
 next_state <= S2;
 end if;
 --Piso 2 a Piso 3
 when subiendo2a3 =>
 if SENSOR_PLANTA(1) = '1' then
 next_state <= S3;
 end if;
 --Piso 3 a Piso 4
 when subiendo3a4 =>
 if SENSOR_PLANTA(0) = '1' then
 next_state <= S4;
 end if;
    -- Bajar --
 --Piso 4 a Piso 3
 when bajando4a3 =>
 if SENSOR_PLANTA(1) = '1' then
 next_state <= S3;
 end if;
 --Piso 3 a Piso 2
  when bajando3a2 =>
 if SENSOR_PLANTA(2) = '1' then
 next_state <= S2;
 end if;
 --Piso 2 a Piso 1
 when bajando2a1 =>
 if SENSOR_PLANTA(3) = '1' then
 next_state <= S1;
 end if;
--Estado de emergencia
when EMERGENCIA =>
if PUSHBUTTON<="0000" then
next_state <= S4;
aux <= "000";
end if;
 when others =>
 next_state <= S1;
 end case;
 end process;
 
 output_decod: process (current_state)
 begin
 bcd <= (OTHERS => '0');
 led <= (OTHERS => '0');
 case current_state is
 --Indicar Piso 1
 when S1 =>
 bcd(3) <= '1';
 Marcha_Paro <='0';
 --Indicar Piso 2
 when S2 =>
 bcd(2) <= '1';
 Marcha_Paro <='0';
 --Indicar Piso 3
 when S3 =>
 bcd(1) <= '1';
 Marcha_Paro <='0';
 --Indicar Piso 4
 when S4 =>
 bcd(0) <= '1';
 Marcha_Paro <='0';
 
 when subiendo1a2 =>
 led(0)<= '1';
 bcd(3)<='1';
 Motor_direccion <= '1';
 Marcha_Paro <= '1';
 
 when subiendo2a3 =>
 led(0)<= '1';
 bcd(2)<='1';
 Motor_direccion <= '1';
 Marcha_Paro <= '1';
 
 when subiendo3a4 =>
 led(0)<= '1';
 bcd(1)<='1';
 Motor_direccion <= '1';
 Marcha_Paro <= '1';
 
 when bajando2a1 =>
 led(1)<= '1';
 bcd(2)<='1';
 Motor_direccion <= '0';
 Marcha_Paro <= '1';
 
 when bajando3a2 =>
 led(1)<= '1';
 bcd(1)<='1';
 Motor_direccion <= '0';
 Marcha_Paro <= '1';
 
 when bajando4a3 =>
 led(1)<= '1';
 bcd(0)<='1';
 Motor_direccion <= '0';
 Marcha_Paro <= '1';
 
 when EMERGENCIA =>
 led(2)<= '1';
 Marcha_Paro<='0';

 when others =>
 bcd <= (OTHERS => '0');
 led <= (OTHERS => '0');
 end case;
 end process;
end behavioral;