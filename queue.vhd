
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity queue is
    Port ( CLK: in std_logic;
           button_in : in STD_LOGIC_VECTOR (3 downto 0);
           button_out : out STD_LOGIC_VECTOR (3 downto 0);
           LED : out STD_LOGIC);
end queue;

architecture Behavioral of queue is
signal aux: integer; 
signal aux_dos: integer:=100;
begin
process (button_in,CLK)
begin
for i in 3 downto 0 loop
if button_in(i)='1' then
aux <= i;
if rising_edge(CLK)then
aux_dos <= aux_dos +1;
end if;
end if;
end loop;

end process;

process (CLK)
begin
aux_dos <= aux_dos - 1;
if aux_dos = 0 then
button_out(aux)<=button_in(aux);
end if;
end process;

end Behavioral;
