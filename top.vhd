library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity top is 
Port (
    Boton_Planta        : in  std_logic_vector(3 downto 0);
    CLK                 : in  std_logic;
    SWITCH_EMERGENCIA   : in  std_logic;
    Sensor_Planta       : in  std_logic_vector(3 downto 0);
    segment             : out std_logic_vector(6 downto 0);
    Led_Motor           : out std_logic_vector(1 downto 0);
    Led_Estado          : out std_logic_vector(2 downto 0);
    digctrl             : out std_logic_vector(7 downto 0)
 );
end top;

architecture Behavioral of top is

component sincronizador
    port(
        CLK : in std_logic;
        ASYNC_IN : in std_logic;
        SYNC_OUT : out std_logic
    );
end component;

component detector_flanco
    port(
         CLK : in std_logic;
         SYNC_IN : in std_logic;
         FLANCO : out std_logic
    );
end component;

component fsm
    port(
        SWITCH_EMERGENCIA : in std_logic;
        CLK : in std_logic;
        SENSOR_PLANTA : in std_logic_vector(3 downto 0);
        PUSHBUTTON : in std_logic_vector(3 downto 0);
        BCD : out std_logic_vector(3 downto 0);
        LED : out std_logic_vector (2 downto 0);
        Motor_Direccion: out std_logic;
        Marcha_Paro: out std_logic
    );
end component;

component control_motor is
port(
  --clk: in std_logic;
  sentido_motor: in std_logic;
  power_motor: in std_logic;
  led_motor: out std_logic_vector(1 downto 0)
);
end component;

component decoder is
    port(
        code: IN std_logic_vector(3 downto 0);
        led: OUT std_logic_vector(6 downto 0)
        );
end component;

signal sync_out, luz, edg: std_logic_vector(3 downto 0);
signal marcha, direccion: std_logic;
begin
Inst_syncb0        : sincronizador    PORT MAP(clk=>clk, ASYNC_IN=>Boton_Planta(0), SYNC_OUT=>sync_out(0) );
Inst_syncb1        : sincronizador    PORT MAP(clk=>clk, ASYNC_IN=>Boton_Planta(1), SYNC_OUT=>sync_out(1) );
Inst_syncb2        : sincronizador    PORT MAP(clk=>clk, ASYNC_IN=>Boton_Planta(2), SYNC_OUT=>sync_out(2) );
Inst_syncb3        : sincronizador    PORT MAP(clk=>clk, ASYNC_IN=>Boton_Planta(3), SYNC_OUT=>sync_out(3) );

Inst_edge0         : detector_flanco  PORT MAP(clk=>clk, SYNC_IN=>SYNC_OUT(0), FLANCO=>edg(0) );
Inst_edge1         : detector_flanco  PORT MAP(clk=>clk, SYNC_IN=>SYNC_OUT(1), FLANCO=>edg(1) );
Inst_edge2         : detector_flanco  PORT MAP(clk=>clk, SYNC_IN=>SYNC_OUT(2), FLANCO=>edg(2) );
Inst_edge3         : detector_flanco  PORT MAP(clk=>clk, SYNC_IN=>SYNC_OUT(3), FLANCO=>edg(3) );

Inst_fsm           : fsm              PORT MAP(SWITCH_EMERGENCIA=>SWITCH_EMERGENCIA, clk=>clk, SENSOR_PLANTA => SENSOR_PLANTA, PUSHBUTTON=>edg, BCD => luz,LED => Led_Estado, Motor_Direccion => direccion, MARCHA_PARO => marcha);
Inst_control_motor : control_motor    PORT MAP(sentido_motor => direccion, power_motor => marcha, led_motor => Led_Motor);
Inst_Decoder       : decoder          PORT MAP(code => Luz,led => segment);
digctrl<="11111110";
end Behavioral;
